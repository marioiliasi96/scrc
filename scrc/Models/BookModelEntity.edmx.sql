
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/22/2018 19:36:09
-- Generated from EDMX file: C:\Users\Mario\source\repos\scrc\scrc\Models\BookModelEntity.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BooksDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__Books__DetailsId__47DBAE45]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Books] DROP CONSTRAINT [FK__Books__DetailsId__47DBAE45];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Books]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Books];
GO
IF OBJECT_ID(N'[dbo].[BooksDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BooksDetails];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Books'
CREATE TABLE [dbo].[Books] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(100)  NOT NULL,
    [Author] nvarchar(100)  NOT NULL,
    [DatePublished] datetime  NOT NULL,
    [DetailsId] int  NULL
);
GO

-- Creating table 'BooksDetails'
CREATE TABLE [dbo].[BooksDetails] (
    [Id] int  NOT NULL,
    [Details] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Books'
ALTER TABLE [dbo].[Books]
ADD CONSTRAINT [PK_Books]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BooksDetails'
ALTER TABLE [dbo].[BooksDetails]
ADD CONSTRAINT [PK_BooksDetails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [DetailsId] in table 'Books'
ALTER TABLE [dbo].[Books]
ADD CONSTRAINT [FK__Books__DetailsId__47DBAE45]
    FOREIGN KEY ([DetailsId])
    REFERENCES [dbo].[BooksDetails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Books__DetailsId__47DBAE45'
CREATE INDEX [IX_FK__Books__DetailsId__47DBAE45]
ON [dbo].[Books]
    ([DetailsId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------